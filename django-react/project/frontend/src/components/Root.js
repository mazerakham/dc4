import React from 'react';
import App from './App';
import Socket from 'client/socket'


export default class Root extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.client = Socket();

    this.buttonHandler = () => {
      this.client.sendButtonClientEvent((ret) => {
        console.log("sendButtonClientEvent callback with ret " + ret);
      })
    }

    this.client.registerButtonServerEventHandler((data) => {
      console.log("Button Server Event handler called with arg " + data)
      this.refs.App.handleButtonEvent();
    })
  }

  render() {
    return (
      <div>
        <h2>Root component (rendering App below)</h2>
        <App buttonHandler={this.buttonHandler} ref="App"/>
      </div>
    );
  }
}
