import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {x: 42, userClicked: false};
  }

  handleButtonEvent(){
    console.log("App handling button event.")
    this.setState({...this.state, userClicked: true});
  }




  render(props) {
    console.log("App render function called.");

    let message;
    if(this.state.userClicked) {
      message = (
        <p style={redStyle}>ZOMG SOMEONE CLICKED</p>
      );
    } else {
      message = (
        <p>No one clicked.</p>
      );
    }

    return(
      <div>
        <h3>{message}</h3>
        <button onClick={this.props.buttonHandler}>click me, I'm chill.</button>
      </div>
    )
  }
}

const redStyle = {
    color: 'red',
    height: '15'
}
