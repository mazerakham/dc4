const server = require('http').createServer();
const io = require('socket.io')(server);

io.on('connection', function(client) {

  console.log('client connected...', client.id);

  client.on('buttonClientEvent', function(data, cb) {
    console.log('client clicked a button with data ' + data + ', zomg I have to do something!');
    console.log(data);
    console.log(cb);
    cb(data);
    console.log('Now going to broadcast an event.');
    io.emit('buttonServerEvent', 43)
  });

  client.on('disconnect', function() {
    console.log('client disconnect...', client.id);
  });

  client.on('error', function(err){
    console.log('received error from client:', client.id);
    console.log(err);
  });
});

server.listen(3000, function(err) {
  if (err) throw err;
  console.log('listening on port 3000');
});
