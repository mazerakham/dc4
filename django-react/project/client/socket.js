const io = require('socket.io-client');

export default function() {

  const socket = io.connect('http://localhost:3000');

  function registerButtonServerEventHandler(onButtonServerEventReceived) {
    // This would be a button-click event broadcast from the server.
    socket.on('buttonServerEvent', onButtonServerEventReceived);
  }

  function sendButtonClientEvent(cb) {
    // Send a button-click event to the server.
    socket.emit('buttonClientEvent', 42, cb);
  }

  return {
    registerButtonServerEventHandler,
    sendButtonClientEvent
  }
}
