Followed https://www.valentinog.com/blog/drf/ to get the django-react part
of this working.  

The best way to run this is as follows: from the top-level folder
(django-react-sockets-skeleton), use:

// Django stuff. <br/>
cd project <br/>
python3 -m venv VenvDjango <br/>
source VenvDjango/bin/activate <br/>
pip install --upgrade pip <br/>
pip install django <br/>
pip install django-rest-framework <br/>
python manage.py makemigrations leads <br/>
python manage.py migrate <br/>
python manage.py runserver <br/>

// Node.js backend stuff <br/>
npm i <br/>
npm run server <br/>
  
