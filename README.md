# dc4
Deep Connect 4: My implementation of Google's Alpha Zero algorithm for connect 4.  

All work on this project will be documented on my DC4 journal, which anyone may view:
https://docs.google.com/document/d/1vPzhcgah-UjIeCAHUslsWxeME_3hUW_aFSqbSVhafl4/edit?usp=sharing

The plan: have a front-end that allows users to make moves against an AI in a web-based GUI.  Moves are processed in a back-end that contains an AI trained according to Google's Alpha Zero algorithm.  In this way, humans enjoy the experience of playing against a strong AI.  

Later: Allow humans to choose between multiple agents, so that there is a variety of "difficulty levels".  

Later: Allow humans to plug in a position and have an agent analyze the position by providing best moves and move-sequences.  
